== Lista de bebidas

// Separadas en con/sin alcohol
// Ordenadas por orden alfabético

=== Con alcohol

* Cerveza
* Vino tinto
* Ron Barcelo
* Ginebra
* Whisky
* Ladrón de Manzanas

=== Sin alcohol

* Fanta de Naranja
* Cerveza sin
* Seven-Up
* Coca Cola
* Fanta
* Zumo de naranja
* Zumo de piña
